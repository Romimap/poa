using Godot;
using System;

public class Main : Node2D {
    public override void _Ready() {
        Graph g = new Graph();

        g.AddNode("A", 1);
        g.AddNode("B", 1);
        g.AddNode("C", 1);
        g.AddNode("D", 1);

        g.AddLink("A", "B", 1);
        g.AddLink("B", "A", 1);
        g.AddLink("A", "C", 1);
        g.AddLink("D", "A", 1);

        // g.AddNode("A", 1);
        // g.AddNode("B", 1);
        // g.AddNode("C", 1);
        // g.AddNode("D", 1);
        // g.AddNode("E", 1);
        // g.AddNode("F", 1);
        // g.AddNode("G", 1);
        // g.AddNode("H", 1);
        // g.AddNode("I", 1);
        // g.AddNode("J", 1);

        // g.AddLink("A", "C", 1);
        // g.AddLink("A", "D", 1);
        // g.AddLink("A", "F", 1);
        // g.AddLink("B", "D", 1);
        // g.AddLink("B", "E", 1);
        // g.AddLink("B", "G", 1);
        // g.AddLink("C", "E", 1);
        // g.AddLink("C", "H", 1);
        // g.AddLink("D", "I", 1);
        // g.AddLink("E", "J", 1);
        // g.AddLink("F", "G", 1);
        // g.AddLink("F", "J", 1);
        // g.AddLink("G", "H", 1);
        // g.AddLink("H", "I", 1);
        // g.AddLink("I", "J", 1);

        // GD.Print(g);

        // g.FindStables();   
        g.AttackFor(10); 
    }
}

using System;
using System.Collections.Generic;
using Godot;


//déf d'une class graph
public class Graph
{
    public Dictionary<string, GraphNode> nodes = new Dictionary<string, GraphNode>();


    //ajoutte un noued, 
    public void AddNode(string name, float force)
    {
        GraphNode node = new GraphNode(name, force);
        nodes.Add(name, node);
    }

    //ajoute une arète
    public void AddLink(string from, string to, float force)
    {
        GraphNode fromNode = nodes[from];
        GraphNode toNode = nodes[to];
        fromNode.AddNeighbour(toNode, force);
    }

    //permet de copier le graphe
    public Graph GetClone()
    {
        Graph clone = new Graph();

        // Copy the nodes without the links
        foreach (KeyValuePair<string, GraphNode> entry in nodes)
        {
            GraphNode node = entry.Value;
            clone.AddNode(node.name, node.force);
        }

        // Recreate the links
        foreach (KeyValuePair<string, GraphNode> entry in nodes)
        {
            GraphNode node = entry.Value;
            GraphNode clonedNode = clone.nodes[node.name]; // Cloned node 'from'
            foreach (GraphLink link in node.neighbours)
            {
                GraphNode clonedNeighbour = clone.nodes[link.to.name]; // Cloned node 'to'
                clonedNode.AddNeighbour(clonedNeighbour, link.force);
            }
        }

        return clone;
    }

    public void AttackFor(int iterations)
    {
        Graph g = GetClone();

        for (int i = 0; i < iterations; i++)
        {
            foreach (KeyValuePair<string, GraphNode> entry in nodes)
            {
                GraphNode node = entry.Value;
                node.Attack();
            }

            foreach (KeyValuePair<string, GraphNode> entry in nodes)
            {
                GraphNode node = entry.Value;
                node.ApplyCriticism();
            }

            GD.Print("Iteration: " + (i+1));
            GD.Print(GetIterationCriticism());
        }

    }

    //fonction pour chercher la liste des stables
    public List<Stable> FindStables()
    {
        List<Stable> stables = new List<Stable>();

        Graph g = GetClone();

        List<GraphNode> toCompute = new List<GraphNode>();
        foreach (KeyValuePair<string, GraphNode> kvp in g.nodes)
        {
            toCompute.Add(kvp.Value);
        }

        FindStableRec(toCompute, new List<GraphNode>(), stables, g);

        return stables;
    }

    public void FindStableRec(List<GraphNode> toCompute, List<GraphNode> currentStable, List<Stable> stables, Graph g)
    {
        bool deadEnd = true;
        //Pour chaque sous etat
        foreach (GraphNode nextGraphNode in toCompute)
        {
            //On regarde si le noeud est voisin du stable actuel
            bool isNeighbour = false;
            foreach (GraphNode currentNode in currentStable)
            {
                if (nextGraphNode.isNeighbour(currentNode))
                {
                    isNeighbour = true;
                    break;
                }
            }
            //Si il n'est pas voisin, on peut l'ajouter
            if (!isNeighbour)
            {
                deadEnd = false;

                //On ajoute aux noeuds à traiter que les noeuds alphabetiquement superieurs au noeud traité
                List<GraphNode> nextCompute = new List<GraphNode>();
                foreach (GraphNode nextComputeNode in toCompute)
                {
                    if (StringComparer.OrdinalIgnoreCase.Compare(nextGraphNode.name, nextComputeNode.name) < 0)
                    {
                        nextCompute.Add(nextComputeNode);
                    }
                }
                nextCompute.Remove(nextGraphNode);

                //On clone le stable actuel
                List<GraphNode> nextStable = new List<GraphNode>(currentStable);
                nextStable.Add(nextGraphNode);
                FindStableRec(nextCompute, nextStable, stables, g);
            }
        }
        if (deadEnd)
        {
            Stable s = new Stable(currentStable);
            stables.Add(s);
            GD.Print(s);

            for (int i = 0; i < s.arguments.Count; i++)
            {
                for (int j = i + 1; j < s.arguments.Count; j++)
                {
                    g.AddLink(s.arguments[i].name, s.arguments[j].name, 1);
                }
            }
        }
        return;
    }

    public string GetIterationCriticism() {
        string toReturn = "";
        foreach (KeyValuePair<string, GraphNode> entry in nodes) {
            GraphNode node = entry.Value;
            toReturn += node.name + " \t Criticism: " + node.criticism.ToString("0.000") + " \t Attack value: " + node.GetAttackValue().ToString("0.000") + "\n";
        }
        return toReturn;
    }

    public override string ToString()
    {
        string toReturn = "####################\n";
        toReturn += "Graph: " + nodes.Count + " nodes.\n";
        toReturn += "List = { ";
        foreach (KeyValuePair<string, GraphNode> entry in nodes)
        {
            toReturn += entry.Key + " ";
        }
        toReturn += "}\n";
        foreach (KeyValuePair<string, GraphNode> entry in nodes)
        {
            GraphNode node = entry.Value;
            toReturn += node.ToString();
        }
        toReturn += "####################\n";
        return toReturn;
    }
}

public class Stable
{
    public List<GraphNode> arguments = new List<GraphNode>();

    public Stable(List<GraphNode> arguments)
    {
        this.arguments = arguments;
    }

    public override string ToString()
    {
        string ans = "";

        foreach (GraphNode graphNode in arguments)
        {
            ans += graphNode.name + " ";
        }

        return ans;
    }
}

//def un noeud, ses voisins (liste) et sa force
public class GraphNode
{
    public string name;
    public List<GraphLink> neighbours = new List<GraphLink>();
    public float force;
    public float criticism = 0;
    public float criticismTurn = 0;

    //permet au noeud de donner leur nom et leur poid
    public GraphNode(string name, float force)
    {
        this.name = name;
        this.force = force;
    }

    //permet d'ajouter un voisin dans la liste du noeud.
    public void AddNeighbour(GraphNode neighbour, float force)
    {
        //if (isNeighbour(neighbour)) return;

        GraphLink link = new GraphLink(this, neighbour, force);
        neighbours.Add(link);
    }

    //permet de tester si deux noeuds sont voisins: comme le graphe est orienté il faut vérifier si l'un des deux noeuds a l'autre comme voisin
    public bool isNeighbour(GraphNode graphNode)
    {
        foreach (GraphLink link in neighbours)
        {
            if (link.to == graphNode) return true;
        }

        foreach (GraphLink link in graphNode.neighbours)
        {
            if (link.to == this) return true;
        }

        return false;
    }

    public override string ToString()
    {
        string toReturn = "Node: " + name + ", force: " + force + " {\n";
        foreach (GraphLink neighbour in neighbours)
        {
            toReturn += "\t" + neighbour.ToString();
        }
        toReturn += "}\n";
        return toReturn;
    }

    public void Attack()
    {
        foreach (GraphLink graphLink in neighbours)
        {
            float attackValue = graphLink.force * GetAttackValue();
            graphLink.to.criticismTurn += attackValue;
        }

    }

    public void ApplyCriticism()
    {
        criticism += criticismTurn;
        criticismTurn = 0;
    }

    public float GetAttackValue () {
        return force * Mathf.Exp(-criticism);
    }
}

//déf une arète.
public class GraphLink
{
    public GraphNode from;
    public GraphNode to;
    public float force;

    public GraphLink(GraphNode from, GraphNode to, float force)
    {
        this.from = from;
        this.to = to;
        this.force = force;
    }

    public override string ToString()
    {
        return from.name + " ---" + force + "--> " + to.name + "\n";
    }
}